from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.client import ServerProxy
from dataclasses import dataclass
from threading import Lock, Condition
from socketserver import ThreadingMixIn
from traceback import print_exc
from klepto.archives import dir_archive

class DurableLog:
    def __init__(self, path):
        self._path = path
        self._file = open(self._path, "a+")
        self._lock = Lock()

    def append(self, x):
        with self._lock:
            self._file.write(x)
            self._file.write("\n")
            self._file.flush()

    def cold_restart(self):
        # Recovers non dumped data from log
        model = {}
        with self._lock:
            with open(self._path, "r") as f:
                for line in f.readlines():
                    x = line.split()
                    if x[0] == "write":
                        model[x[1]] = x[2]
                    elif x[0] == "dump":
                        model = {}
        return model

class DatumLock:
    S_LOCK = True
    E_LOCK = False
    TIMEOUT = 5

    def __init__(self):
        self.lock = Condition(lock=Lock())
        # self.write_condition = Condition(lock=self.lock)
        self.type = None # this field requires locking
        self.tids = set() # this field can be accessed without lock

    def acquire_shared(self, tid):
        if not self.lock.acquire(timeout=self.TIMEOUT):
            return False
        self.type = self.S_LOCK
        self.tids.add(tid)
        self.lock.release()
        return True

    def acquire_exclusive(self, tid):
        # acquire the lock
        if not self.lock.acquire(timeout=self.TIMEOUT):
            return False
            # if the lock is free or tid is the only owner (of the
            # shared lock) we are done
        def condition():
            if len(self.tids) == 0 or self.tids == {tid}:
                self.type = self.E_LOCK
                self.tids.add(tid)
                return True
            return False
        # wait for release notifications
        res = self.lock.wait_for(condition, timeout=self.TIMEOUT)
        if not res:
            self.lock.release()
        return res

    def release(self, tid):
        # prevent any unknown tid to do anything
        if tid not in self.tids:
            return False
        # if lock is shared acquire it first
        if self.type == self.S_LOCK:
            if not self.lock.acquire(timeout=self.TIMEOUT):
                return False
        # unregister, notify and release
        self.tids.remove(tid)
        self.lock.notify_all()
        self.lock.release()
        return True

def as_node(addr):
    return ServerProxy(addr, allow_none=True)

class Node(object):
    def __init__(self, address, model_log_path="model.log", db_store_dir="data"):
        self._address = address
        self.nodes = {}
        self.durable_log = DurableLog(model_log_path)
        self.model = dir_archive(db_store_dir)
        self.model.update(self.durable_log.cold_restart())
        self.locks = {}
        self.global_lock = Lock()
        self.buffers = {}

    def initalize(self, entrypoint=None):
        if not entrypoint:
            return
        i = 0
        known = [entrypoint]
        while i < len(known):
            a = known[i]
            if a not in known[:i]:
                print(f"New node discovered: {a}")
                known += as_node(a).hello(self._address)
            i += 1
        with self.global_lock:
            self.nodes = {a: as_node(a) for a in known}

    def hello(self, address):
        with self.global_lock:
            res = list(self.nodes.keys())
            print(f"Hello from: {address}, sending: {res}")
            self.nodes[address] = as_node(address)
        return res

    def read(self, x):
        print(f"Read request for {x}")
        if x in self.model:
            return self.model[x]
        elif x in self.model.archive:
            return self.model.archive[x]
        return 0

    def write(self, k, v):
        print(f"Write request for {k} := {v}")
        self.durable_log.append(f"write {k} {v}")
        self.model[k] = v
        if len(self.model) >= 100:
            self.model.dump()
            self.durable_log.append("dump")
            ks = [x for x in self.model]
            for x in ks:
                del self.model[x]
        return v

    def lock_info(self):
        lock_type_name = lambda x: "shared" if x == DatumLock.S_LOCK else "exclusive"
        res = {k: (lock_type_name(self.locks[k].type), list(self.locks[k].tids))
               for k in self.locks if len(self.locks[k].tids) != 0}
        return res

    def lock_datum(self, k, tid, shared=False):
        lock_type = "Shared" if shared else "Exclusive"
        print(f"{lock_type} lock request for {k} in {tid}")
        with self.global_lock:
            if k not in self.locks:
                self.locks[k] = DatumLock()
        if shared:
            return self.locks[k].acquire_shared(tid)
        else:
            return self.locks[k].acquire_exclusive(tid)

    def unlock_datum(self, k, tid):
        print(f"Unlock request for {k} in {tid}")
        return self.locks[k].release(tid)

    def safe_query(self, q, max_attempts_no=50):
        errors = []
        try:
            return self.query(q)
        except Exception as e:
            from time import sleep
            t = Transaction.parse(q)
            q = ["begin", t.id, "abort"]
            i = 0
            done = False
            errors.append(e)
            self.durable_log.append(f"rollback {t.id}")
            while not done and i < max_attempts_no:
                sleep(0.5)
                try:
                    self.query(q)
                    done = True
                except Exception as e1:
                    errors.append(e1)
                    i += 1
            if not done:
                self.durable_log.append(f"critical {t.id}")
                print(f"CRITICAL ERROR! Could not abort transaction {t.id} due to {str(errors)}")
                print_exc()
                raise Exception(f"CRITICAL SERVER ERROR! {str(errors)}")
        raise Exception(f"Server error, transaction aborted! {str(errors)}")

    def query(self, q):
        t = Transaction.parse(q)
        # allocate resources
        if t.id not in self.buffers:
            self.buffers[t.id] = {
                "locked": list(),
                "stack": list(),
                "model": dict()
            }
        stack = self.buffers[t.id]["stack"]
        locked = self.buffers[t.id]["locked"]
        model = self.buffers[t.id]["model"]
        # acquire locks
        for h, k, shared in t.keys():
            lock_type = "shared" if shared else "exclusive"
            locked_shared = (h, k, True) in locked
            locked_exclusive = (h, k, False) in locked
            # if not already acquired
            if (shared and not (locked_shared or locked_exclusive)) or (not shared and not locked_exclusive):
                # needed for lock upgrades
                if locked_shared:
                    locked.remove((h, k, True))
                # request the lock
                n = self.nodes[h] if h != self._address else self
                locked.append((h, k, shared))
                if not n.lock_datum(k, t.id, shared):
                    raise Exception(f"Could not acquire {lock_type} lock {k} for transaction: {t.id}")
                print(f"Acquired {lock_type} lock for {k} in {t.id}")
        print(f"Begin transaction {t.id} \n{self.buffers[t.id]} \n{q}")
        # common definitions
        def release_all_locks():
            for h, k, sh in set(locked):
                n = self.nodes[h] if h != self._address else self
                # release the datum
                res = n.unlock_datum(k, t.id)
                if not res:
                    locked.remove((h, k, sh))
                    raise Exception(f"Cannot release UNACQUIRED lock for {h} {k} in {t.id}")
                # check if it has been released and remove it
                linfo = n.lock_info()
                res = t.id not in linfo[k][1] if k in linfo else True
                if res:
                    locked.remove((h, k, sh))
                else:
                    self.durable_log.append(f"critical {t.id}")
                    raise Exception(f"Cannot release ACQUIRED lock for {h} {k} in {t.id}")
        # perform actions
        for a in t.actions:
            if isinstance(a, Transaction.Push):
                self.durable_log.append(f"transaction {t.id} push {a.value}")
                stack.append(a.value)
            elif isinstance(a, Transaction.Read):
                self.durable_log.append(f"transaction {t.id} read {a.host} {a.key}")
                if (x := (a.host, a.key)) in model:
                    stack.append(model[x])
                else:
                    n = self.nodes[a.host] if a.host != self._address else self
                    stack.append(n.read(a.key))
            elif isinstance(a, Transaction.Write):
                self.durable_log.append(f"transaction {t.id} write {a.host} {a.key} {stack[-1]}")
                model[(a.host, a.key)] = stack.pop()
            elif isinstance(a, Transaction.Commit):
                self.durable_log.append(f"transaction {t.id} commit")
                print(f"Commit transaction {t.id}\n{self.buffers[t.id]}")
                for h, k in model:
                    n = self.nodes[h] if h != self._address else self
                    n.write(k, model[(h, k)])
                release_all_locks()
                del self.buffers[t.id]
                break
            elif isinstance(a, Transaction.Abort):
                self.durable_log.append(f"transaction {t.id} abort")
                print(f"Abort transaction {t.id}\n{self.buffers[t.id]}")
                release_all_locks()
                del self.buffers[t.id]
                return "Aborted!"
        return stack

class Transaction:
    @dataclass
    class Read:
        host = ""
        key = ""

    @dataclass
    class Begin:
        value = ""

    @dataclass
    class Push:
        value = ""

    @dataclass
    class Write:
        host = ""
        key = ""

    @dataclass
    class Commit:
        pass

    @dataclass
    class Abort:
        pass

    def __init__(self, tid, actions):
        self.actions = actions
        self.id = tid

    def keys(self):
        shared = []
        exclusive = []
        for a in self.actions:
            if isinstance(a, Transaction.Read) and (x := (a.host, a.key)) not in shared:
                shared.append(x)
            elif isinstance(a, Transaction.Write) and (x := (a.host, a.key)) not in exclusive:
                exclusive.append(x)
        return [(h, k, False) for h, k in exclusive] + [(h, k, True) for h, k in shared if (h, k) not in exclusive]

    def parse(q):
        def commit(Q, a): a.append(Transaction.Commit())
        def abort(Q, a): a.append(Transaction.Abort())
        def begin(Q, a):
            res = Transaction.Begin()
            res.value = next(Q)
            a.append(res)
        def push(Q, a):
            res = Transaction.Push()
            res.value = next(Q)
            a.append(res)
        def read(Q, a):
            res = Transaction.Read()
            res.host = next(Q)
            res.key = next(Q)
            a.append(res)
        def write(Q, a):
            res = Transaction.Write()
            res.host = next(Q)
            res.key = next(Q)
            a.append(res)

        parse_action = {
            "commit": commit,
            "abort": abort,
            "push": push,
            "read": read,
            "write": write,
            "begin": begin
        }
        acts = []
        Q = iter(q)
        tid = 0
        for x in Q:
            parse_action[x](Q, acts)
            if isinstance(a := acts[-1], Transaction.Begin):
                tid = a.value
        return Transaction(tid, acts)

class DBNodeServer(ThreadingMixIn, SimpleXMLRPCServer):

    def __init__(self, node, *args):
        SimpleXMLRPCServer.__init__(self, *args)
        self.node = node
        self.quit = False
        self.allow_none = True

    def serve_forever(self):
        print("DB Server Node up and running...")
        try:
            while not self.quit:
                self.handle_request()
        finally:
            print("DB Server terminated.")

def main():
    import threading
    import argparse

    parser = argparse.ArgumentParser(description='DB Distributed DBMS')
    parser.add_argument("address")
    parser.add_argument("port", type=int)
    parser.add_argument("--entrypoint")
    parser.add_argument("--log-file")
    parser.add_argument("--db-dir")
    args = parser.parse_args()

    node = Node(f"http://{args.address}:{args.port}",
                model_log_path=args.log_file,
                db_store_dir=args.db_dir)

    with DBNodeServer(node, (args.address, args.port)) as server:
        server.register_introspection_functions()
        server.register_function(node.hello)
        server.register_function(node.read)
        server.register_function(node.write)
        server.register_function(node.lock_info)
        server.register_function(node.lock_datum)
        server.register_function(node.unlock_datum)
        server.register_function(node.safe_query)
        threading.Thread(target=node.initalize, args=[args.entrypoint]).start()
        server.serve_forever()

if __name__ == "__main__":
    main()
