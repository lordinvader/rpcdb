from cmd import Cmd
from xmlrpc.client import ServerProxy

class DBClient(Cmd):
    prompt = " db > "
    intro = " Welcome! "

    def __init__(self, *args, **kwargs):
        Cmd.__init__(self, *args, **kwargs)
        self._user = None
        self._host = None
        self._buffer = ""
        self._tid = 0

    def _refresh_prompt(self):
        self.prompt = f" [{self._tid}] {self._user}@{self._host} > "

    def send(self):
        n = ServerProxy(self._host)
        q = f"begin {self._user}.{self._tid} \n{self._buffer}"
        res = n.safe_query(q.split())
        self._buffer = ""
        if "commit" in q.split():
            self._tid += 1
            self._refresh_prompt()
        return res

    def buffer(self, line):
        self._buffer += f"{line} \n"

    def force_exclusive_lock(self, k, host=None):
        if not host:
            host = self._host
        self.buffer(f"read {host} {k}")
        self.buffer(f"write {host} {k}")
        self.send()

    def table_fields(self, table_name, host=None):
        host = self._host if host is None else host
        self.buffer(f"read {host} schema.table.{table_name}.fields")
        return str(self.send()[-1]).split(",")

    def table_rows(self, table_name, host=None):
        host = self._host if host is None else host
        # get row number and fields
        self.buffer(f"read {host} schema.table.{table_name}.rows")
        row_no = int(self.send()[-1]) + 1
        fields = self.table_fields(table_name, host=host)
        # iterate rows
        for i in range(1, row_no):
            for f in fields:
                self.buffer(f"read {host} {table_name}.{i}.{f}")
            row = self.send()[-len(fields):]
            yield [str(i)] + row

    def table_insert_row(self, table_name, row, host=None):
        host = self._host if host is None else host
        self.buffer(f"read {host} schema.table.{table_name}.rows")
        row_no = int(self.send()[-1]) + 1
        fields = self.table_fields(table_name, host=host)
        assert(len(row) == len(fields))
        # write the row
        for f, x in zip(fields, row):
            self.buffer(f"push {x}")
            self.buffer(f"write {host} {table_name}.{row_no}.{f}")
        # save row number
        self.buffer(f"push {row_no}")
        self.buffer(f"write {host} schema.table.{table_name}.rows")
        self.send()

    def do_exit(self, x):
        print("Bye!")
        return True

    do_EOF = do_exit

    def do_show(self, x):
        print(self._buffer)

    def do_host(self, h):
        self._host = h
        self._refresh_prompt()

    def do_user(self, u):
        self._user = u
        self._refresh_prompt()

    def do_clear(self, u):
        self._buffer = ""

    def do_commit(self, x):
        self.buffer("commit")
        self.do_send(x)

    def do_abort(self, x):
        self.buffer("abort")
        self.do_send(x)

    def do_send(self, x):
        try:
            print(self.send())
        except KeyboardInterrupt:
            return None
        except Exception as e:
            print("Error occured!")
            print(e)

    def do_lock_info(self, x):
        n = ServerProxy(self._host)
        print(n.lock_info())

    def do_table_create(self, x):
        xs = x.split()
        host = xs[0]
        table_name = xs[1]
        table_fields = xs[2:]
        assert(len(table_fields) > 0)
        assert(table_name != "schema")
        # acquire schema and table locks
        self.force_exclusive_lock("schema")
        self.force_exclusive_lock(f"{table_name}")
        # register table name
        self.buffer(f"read {host} schema.tables")
        self.do_show(None)
        res = str(self.send()[-1])
        if res == "0" or res == "":
            self.buffer(f"push {table_name}")
            self.buffer(f"write {host} schema.tables")
        elif table_name not in res.split(','):
            self.buffer(f"push {res},{table_name}")
            self.buffer(f"write {host} schema.tables")
        # register table fields
        comma_fields = ",".join(table_fields)
        self.buffer(f"push {comma_fields}")
        self.buffer(f"write {host} schema.table.{table_name}.fields")
        # register rows count
        self.buffer(f"read {host} schema.table.{table_name}.rows")
        self.do_show(None)
        res = str(self.send()[-1])
        self.buffer(f"push {res}")
        self.buffer(f"write {host} schema.table.{table_name}.rows")
        self.do_show(None)
        self.do_commit(None)

    def do_table_insert(self, x):
        xs = x.split()
        host = xs[0]
        table_name = xs[1]
        row = xs[2:]
        try:
            # acquire table lock
            self.force_exclusive_lock(f"{table_name}")
            self.table_insert_row(table_name, row, host=host)
        finally:
            self.do_commit(None)

    def do_table_show(self, x):
        xs = x.split()
        host = xs[0]
        table_name = xs[1]
        try:
            # acquire table lock
            self.force_exclusive_lock(f"{table_name}")
            # get fields
            fields = self.table_fields(table_name, host=host)
            fields = ["#"] + fields
            # print the table
            header = "\t | \t".join(fields)
            spaces = "-"*(len(header) + 16*(len(fields) - 1))
            print(f"Table: {table_name} \n {header} \n{spaces}")
            for r in self.table_rows(table_name, host=host):
                row = "\t   \t".join(r)
                print(row)
        finally:
            self.buffer("commit")
            self.send()

    def default(self, line):
        self.buffer(line)
        print(self._buffer)

if __name__ == "__main__":
    from sys import argv
    c = DBClient()
    if len(argv) >= 3:
        c.do_user(argv[1])
        c.do_host(argv[2])
    c.cmdloop()
