from random import randrange as rrange
from client import DBClient
from time import sleep

banks = ["http://localhost:8080", "http://localhost:8081"]

def amount(client, user="John"):
    print(f"\nAMOUNT {user}\n")
    for b in banks:
        client.buffer(f"read {b} {user}")
    client.buffer("commit")
    client.do_show(None)
    r = client.send()
    print(r)
    tot = sum(map(int, r))
    print(tot)
    assert(tot >= 0)

def deposit(client, user="John", amount=1, bank=None):
    print(f"\nDEPOSIT {user} {bank} {amount}\n")
    client.buffer(f"read {bank} {user}")
    client.do_show(None)
    r1 = client.send()
    print(r1)
    client.buffer(f"push {int(r1[0]) + amount}")
    client.buffer(f"write {bank} {user}")
    client.buffer("commit")
    client.do_show(None)
    print(client.send())

def withdraw(client, user="John", amount=1):
    print(f"\nWITHDRAW {user} {amount}\n")
    for b in banks:
        client.buffer(f"read {b} {user}")
    client.do_show(None)
    r1 = client.send()
    print(r1)
    tot = sum(map(int, r1))
    if tot >= amount:
        banks_amount = {b: int(a) for b, a in zip(banks, r1)}
        n = amount
        for b in banks_amount:
            if n == 0:
                break
            a = banks_amount[b]
            if a <= n:
                client.buffer("push 0")
                client.buffer(f"write {b} {user}")
                n -= a
            else:
                client.buffer(f"push {a-n}")
                client.buffer(f"write {b} {user}")
                n = 0
        client.buffer("commit")
        client.do_show(None)
        print(client.send())
    else:
        client.do_abort(None)

def t1(user="t1", tid=0):
    while True:
        b = banks[rrange(0, len(banks))]
        print(f"Node is: {b}")
        c = DBClient()
        c.do_host(b)
        c.do_user(user)
        c._tid = tid
        try:
            amount(c)
            withdraw(c, amount=rrange(1, 100))
            amount(c)
            sleep(rrange(1, 3))
        except KeyboardInterrupt:
            return None
        except Exception as e:
            print(f"T1 FAILED! ({str(e)}) Retrying...")
            sleep(rrange(6, 10))
        finally:
            tid += 1

def t2(user="t2", tid=0):
    while True:
        b1 = banks[rrange(0, len(banks))]
        b2 = banks[rrange(0, len(banks))]
        print(f"Node is: {b1}")
        c = DBClient()
        c.do_host(b1)
        c.do_user(user)
        c._tid = tid
        try:
            amount(c)
            deposit(c, amount=rrange(1, 100), bank=b2)
            amount(c)
            sleep(rrange(1, 3))
        except KeyboardInterrupt:
            return None
        except Exception as e:
            print(f"T2 FAILED! ({str(e)}) Retrying...")
            sleep(rrange(6, 10))
        finally:
            tid += 1

# th1 = th.Thread(target=t1)
# th2 = th.Thread(target=t2)

# for t in [th1, th2, th3]:
#     t.start()

# for t in [th1, th2, th3]:
#     t.join()
